package com.sibedge.yokodzun.common.data


data class Rate(
        val battleId: String = "",
        val sectionId: String = "",
        val yokodzunId: String = "",
        val parameterId: String = "",
        val raterCode: String = "",
        val value: Float = 0f
)